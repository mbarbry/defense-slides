from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

ft = 60

fig1 = plt.figure(1, figsize=(12.5, 5))
ax1 = fig1.add_subplot(111)

circle1 = plt.Circle((-3.5, 0), 1, color='#2ca02c')
circle2 = plt.Circle((3.5, 0), 1, color='#2ca02c')

ax1.add_artist(circle1)
ax1.add_artist(circle2)

ax1.text(4.75, -0.1, r"$+$", fontsize=ft, color="#d62728")
ax1.text(1.6, -0.1, r"$-$", fontsize=ft, color="#1f77b4")

ax1.text(-2.25, -0.1, r"$+$", fontsize=ft, color="#d62728")
ax1.text(-5.5, -0.1, r"$-$", fontsize=ft, color="#1f77b4")
ax1.text(-0.5, 1.5, r"BDP", fontsize=ft)

ax1.set_axis_off()
ax1.set_xlim([-5.0, 5.0])
ax1.set_ylim([-2, 2.0])
fig1.savefig("BDP.svg", format="svg", bbox_inches='tight')


fig2 = plt.figure(2, figsize=(12.5, 5))
ax1 = fig2.add_subplot(111)

circle1 = plt.Circle((-1.5, 0), 1, color='#2ca02c')
circle2 = plt.Circle((1.5, 0), 1, color='#2ca02c')

ax1.add_artist(circle1)
ax1.add_artist(circle2)

ax1.text(2.75, -0.1, r"$+$", fontsize=ft, color="#d62728")
#ax1.text(1.6, -0.1, r"$-$", fontsize=ft, color="#1f77b4")

#ax1.text(-2.25, -0.1, r"$+$", fontsize=ft, color="#d62728")
ax1.text(-3.5, -0.1, r"$-$", fontsize=ft, color="#1f77b4")
ax1.text(-0.9, 1.5, r"CTP", fontsize=ft)

ax1.set_axis_off()
ax1.set_xlim([-5.0, 5.0])
ax1.set_ylim([-2, 2.0])
fig2.savefig("CTP.svg", format="svg", bbox_inches='tight')

fig3 = plt.figure(3, figsize=(12.5, 5))
ax1 = fig3.add_subplot(111)

circle1 = plt.Circle((-1.5, 0), 1, color='#2ca02c')
circle2 = plt.Circle((1.5, 0), 1, color='#2ca02c')
ax1.add_artist(circle1)
ax1.add_artist(circle2)

ax1.text(2.75, -0.1, r"$+$", fontsize=ft, color="#d62728")
#ax1.text(1.6, -0.1, r"$-$", fontsize=ft, color="#1f77b4")

#ax1.text(-2.25, -0.1, r"$+$", fontsize=ft, color="#d62728")
ax1.text(-3.5, -0.1, r"$-$", fontsize=ft, color="#1f77b4")
ax1.text(-0.5, 1.5, r"QP", fontsize=ft)

ax1.set_axis_off()
ax1.set_xlim([-5.0, 5.0])
ax1.set_ylim([-2, 2.0])
fig3.savefig("QP.svg", format="svg", bbox_inches='tight')

plt.show()
