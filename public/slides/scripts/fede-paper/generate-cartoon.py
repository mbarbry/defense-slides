from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

ft = 60

fig1 = plt.figure(1, figsize=(15, 15))
ax1 = fig1.add_subplot(111)

ax1.text(-5, 5, r"$-$", fontsize=ft)
ax1.text(-3.5, 5, r"$+$", fontsize=ft)
ax1.text(-2, 5, r"CTP", fontsize=ft)
ax1.text(1, 5, r"CTP", fontsize=ft, color=(1, 1, 0))
ax1.text(3, 5, r"CTP'", fontsize=ft, color=(1, 1, 0))
ax1.text(5, 5, r"CTP'", fontsize=ft)
ax1.text(-5, 3, r"Contact", fontsize=ft, color=(1, 1, 0))
ax1.text(-5, 6, r"point", fontsize=ft, color=(1, 1, 0))

ax1.text(-2, 3, r"BDP", fontsize=ft)
ax1.text(0, 3, r"BDP", fontsize=ft, color=(1, 1, 0))

ax1.text(3, 3, r"Relaxed", fontsize=ft, color=(1, 1, 0))
ax1.text(3, 0, r"Unrelaxed", fontsize=ft, color=(1, 1, 0))

ax1.text(-5, 0, r"Conductive regime", fontsize=ft)
ax1.text(-5, 2, r"Capacitive coupling", fontsize=ft)

ax1.text(-5, -4, r"a)   b)   c)   d)", fontsize=ft)


ax1.set_axis_off()
ax1.set_xlim([-7, 7])
ax1.set_ylim([-7, 7])
fig1.savefig("cartoons.svg", format="svg", bbox_inches='tight')

plt.show()
