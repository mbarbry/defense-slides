from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


for i in range(30):
    arr = mpimg.imread("CO2.{0:02d}.png".format(i))

    mpimg.imsave("resized/CO2-res.{0:02d}.png".format(i), arr[150:150+169, 40:40+370, :])
